#!/usr/bin/env python
def main():
    print "Welcome to the AIMS"    
    

if __name__=="__main__":
    main()

def mean(numbers):
    return float(sum(numbers)) / len(numbers)

def std (x):
    import math
    import numpy as np
    x=np.array(x)
    diff= (x-mean(x))**2
    newvalue= sum (diff)/ float(len(x))
    #sigma= math.sqrt(newvalue)
    return math.sqrt(newvalue)
    
def avg_range(lst=[]):
    avg=0
    som_avrg=0
    for i in range (len(lst)):
        f = open(lst[i],'r')
        lines = [line.strip() for line in f]
        result= lines[-2] 
        new_result=result.split(": ")   
        f.close()
        som_avrg=som_avrg+float(new_result[1])
        avg=som_avrg/float(len(lst))
        return avg
        
