#!/usr/bin/env python
from nose.tools import *
import aims

def test_ints():
    numbers = [1, 2, 3, 4, 5]
    obs = aims.std(numbers)
    exp = 1.414213562
    assert_almost_equal(obs, exp)
    
def test_float():
    numbers = [1., 2., 3., 4., 5.]
    obs = aims.std(numbers)
    exp = 1.414213562
    assert_almost_equal(obs, exp)
    
def test_negative_ints():
    numbers = [-1, -2, -3, -4, -5]
    obs = aims.std(numbers)
    exp = 1.414213562
    assert_almost_equal(obs, exp)
    
def test_negative_float():
    numbers = [-1., -2., -3., -4., -5.]
    obs = aims.std(numbers)
    exp = 1.414213562
    assert_almost_equal(obs, exp)

def test_avg_range():
    obs = aims.avg_range(['data/bert/audioresult-00445'])
    exp = 4.0
    assert_equal(obs, exp)

def test_avg_range():
    obs = aims.avg_range(['data/bert/audioresult-00445','data/bert/audioresult-00270'])
    exp = 2.0
    assert_equal(obs, exp)


def test_avg_range():
    obs = aims.avg_range(['data/bert/audioresult-00445','data/bert/audioresult-00270','data/bert/audioresult-00451','data/bert/audioresult-00286'])
    exp = 1.0
    assert_equal(obs, exp)


